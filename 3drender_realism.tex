\documentclass[serif, 10pt, gray, compress]{beamer}

\usepackage{myBeamerStyle}
\setbeamertemplate{itemize subitem}[default]
\setbeamertemplate{itemize subsubitem}[balls]

\usepackage[sorting=none, backend=bibtex, defernumbers=true]{biblatex}
\addbibresource{bibliography.bib}

\title{MUEI - IGM 2020/21}
\author{Physically-based Rendering}
\date{}

\begin{document}

\frame{
\thispagestyle{empty}

\begin{center}

\ \\% \ \\ \ \\

\includegraphics[width=0.5\textwidth]{images/cgf_cover.jpg}\\

%\ \\ \ \\ \ \\ \ \\
\textbf{Physically-based Rendering (PBR)}\\
Offline rendering of photo-realistic synthetic images
\ \\ \ \\ \ \\ \ \\ \ \\

\begin{scriptsize}
\textbf{Emilio Jos\'e Padr\'on Gonz\'alez}\\emilioj@udc.gal\\ \ \\
MUEI, IGM 2020/21
\end{scriptsize}
\end{center}
}

\AtBeginSection[] {
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection, hideothersubsections]
  \end{frame}
}

\begin{frame}<beamer>
  \frametitle{Outline}
  \tableofcontents[hideothersubsections]
\end{frame}

\section{Introduction}

\begin{frame}[t]
  \frametitle{Physically-based Illumination}
  \framesubtitle{Global Illumination Methods}

  \begin{itemize}
  \item Light modeling: Key topic for realistic results
    \begin{figure}
      \includegraphics[width=0.45\linewidth]{images/sin} \ \ 
      \includegraphics[width=0.45\linewidth]{images/con}
      \vspace*{-0.15cm}
      \caption{Flat vs. illuminated ({\it radiosity}, i.e. only diffuse interreflection)}
    \end{figure}

    \vspace*{-0.15cm}

  \item<2-> Main alternatives
    \begin{itemize}
    \item Ray tracing
      \only<3->{
        \begin{itemize}
        \item[-] Follow {\it 'light rays'} from viewer's eye back to light sources
        \item[-] Very good at simulating different behaviors in
          light-matter interaction
        \end{itemize}
      }
    \item Radiosity
      \only<4>{
        \begin{itemize}
        \item[-] Balance energy in environment, simulating light
          propagation starting at light sources
        \item[-] Only diffuse reflection $\Rightarrow$ Independent of POV
        \end{itemize}
      }
    \end{itemize}
  \end{itemize}
\end{frame}

\frame{
  \frametitle{The Rendering Equation}
  \framesubtitle{Global Illumination Methods}

  \begin{center}
    {\bf Light transport equation} ({\it rendering equation},
    James Kajija 1986)
  \end{center}

  \begin{equation*}\hspace*{-0.5cm}
    L_o(\text{x},\omega_o, \lambda, t) = L_e(\text{x},\omega_o,
    \lambda, t) + \int_{\Omega} f_r(\text{x}, \omega_i, \omega_o,
    \lambda, t) \ L_i(\text{x},\omega_i, \lambda, t) \
    (\omega_i \, \cdot \, \text{n}) \ \text{d}\omega_i
  \end{equation*}

  \begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{images/Rendering_eq}
  \end{figure}
}

\section{Radiosity}

\frame{
  \frametitle{Radiosity}

  \begin{minipage}{0.55\linewidth}
    \begin{figure}
      \begin{equation*}
        B_i = E_i + \rho_i\sum_{j=1}^n B_j F_{ij}, ~ 1 \leq i \leq n%
      \end{equation*}
%      \vspace*{-0.5cm}
%      \setbeamertemplate{caption}{\structure{\insertcaption}}
%      \setbeamertemplate{caption label separator}{}
      \caption{Radiosity Equation and render example}
    \end{figure}
  \end{minipage}
  \begin{minipage}{0.44\linewidth}
    \hspace*{1.2cm} \includegraphics[width=120px]{images/radiosity_sample}
  \end{minipage}

  \begin{block}{Usually}
    \begin{itemize}
    \item Finite elements
    \item Iterative solutions
    \item Key term: \structure{form factor} ($F_{ij}$)
    \end{itemize}
  \end{block}

  \pause

  \vspace*{-0.1cm}
  \begin{alertblock}{Problem}
    \begin{itemize}
    \item {\bf High memory} requirements
    \item Lighting computation {\bf tightly coupled} to the geometry
    \item Non diffuse light?
    \end{itemize}
  \end{alertblock}
}

\frame{
  \frametitle{Radiosity}
  \framesubtitle{Example: Classic Cornell Room}

  \begin{minipage}{0.32\linewidth}
    \begin{figure}
      \includegraphics[width=\linewidth]{images/noilu}
      \caption{No color}
    \end{figure}
  \end{minipage} \
  \begin{minipage}{0.32\linewidth}
    \begin{figure}
      \includegraphics[width=\linewidth]{images/flat}
      \caption{Flat colors}
    \end{figure}
  \end{minipage} \
  \begin{minipage}{0.32\linewidth}
    \begin{figure}
      \includegraphics[width=\linewidth]{images/shaded}
      \caption{Radiosity}
    \end{figure}
  \end{minipage}

  \begin{figure}
    \includegraphics[width=0.33\linewidth]{images/cornell_room_rad.png}
  \end{figure}
}

\section{Ray Tracing}

\frame{
  \frametitle{Basic Ray Tracing}

  \begin{algorithmic}
    \ForAll{pixel in screen}

    \Call{Construct a ray from the eye}{}
    \ForAll{object in the scene}

      \Call{Find Intersection with the ray}{}

      \Call{Keep if closest}{}
    \EndFor
    \EndFor
  \end{algorithmic}

  \begin{figure}
    \includegraphics[width=0.9\textwidth]{images/raytracing}
  \end{figure}
}

\frame{
  \frametitle{Ray Tracing}
  \framesubtitle{Idea: light rays reaching the viewer}

  \begin{figure}
    \includegraphics[width=1.05\textwidth]{images/rtidea1}
  \end{figure}
}

\frame{
  \frametitle{Ray Tracing}
  \framesubtitle{Idea: inverse process, casting rays from viewer}

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=\textwidth]{images/rtidea2}
    \end{figure}
  \end{overprint}
}

\frame{
  \frametitle{Ray Tracing}
  \framesubtitle{Idea: cast new rays from objects to get light and shadows}

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=\textwidth]{images/rtidea3}
    \end{figure}
  \end{overprint}
}

\frame{
  \frametitle{Ray Tracing}
  \framesubtitle{Idea: recursion to get global illumination}

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=\textwidth]{images/rtidea4}
    \end{figure}
    \onslide<2>
    \begin{figure}
      \includegraphics[width=\textwidth]{images/rtidea5}
    \end{figure}
    \onslide<3>
    \begin{figure}
      \includegraphics[width=\textwidth]{images/rtidea6}
    \end{figure}
  \end{overprint}
}

\section{Design of a ray tracer}

\subsection{Introduction}

\frame{
  \frametitle{Photo-realistic rendering \& Ray-Tracing algorithm}

  \begin{itemize}
  \item Most photorealistic rendering systems are based on the
    ray-tracing algorithm
    \begin{itemize}
    \item Very simple basics: following the path of a {\bf ray of
      light} through a scene as it {\bf interacts} with and {\bf
      bounces} off objects in an environment.
    \end{itemize}
  \item Important simulation tasks when implementing a ray tracer:
    \begin{itemize}
    \item Cameras\\
      \only<2>{
        \begin{itemize}
        \item How and from where is the scene being viewed
        \item Cameras generate rays from the viewing point into the scene
        \end{itemize}
      }
    \item Ray-object intersection
      \only<3>{
        \begin{itemize}
        \item Tell precisely where a given ray pierces a geometric object
        \item Determine certain geometric properties of the object at the
          intersection point: normal, material\ldots
        \end{itemize}
      }
    \item Light sources \& light distribution
      \only<4>{
        \begin{itemize}
        \item Key for realistic results
        \item Model the distribution of light throughout the scene:
          \begin{tabbing}
          - locations of lights\\
          - how lights distribute their energy throughout space\\
          \end{tabbing}
        \end{itemize}
      }
    \item Visibility
      \only<5>{
        \begin{itemize}
        \item Energy from a given light is deposited at a point at a
          surface whether there is an uninterrupted path
        \item Easy to solve in a ray tracer: find closest ray-object
          intersection
        \end{itemize}
      }
    \item Surface scattering
      \only<6>{
        \begin{itemize}
        \item Each object must provide information of its appearance,
          e.g.
          \begin{tabbing}
          - how light interacts with the object's surface\\
          - nature of reradiated (scattered) light
          \end{tabbing}
        \item Models for surface scattering are typically parameterized: they
          can simulate a variety of appearances
        \end{itemize}
      }
    \item Indirect lignt transport (recursive ray tracing)
      \only<7>{
        \begin{itemize}
        \item Difference between local and global illumination:
          \begin{tabbing}
            - light can arrive at a surface after \underline{bouncing off} or \underline{passing
              through}\\ several other surfaces
          \end{tabbing}
        \item Trace additional rays originating at the surface
        \item Monte Carlo!
        \end{itemize}
      }
    \item Ray propagation
      \only<8>{
        \begin{itemize}
        \item Light energy only remains constant in a vacuum
        \item What happens to light traveling along a ray as it passes
          through space?
        \item Sophisticated models: fog, smoke\ldots
        \end{itemize}
      }
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Physically Based Rendering}
  \framesubtitle{From Theory to Implementation}

  \begin{itemize}
  \item Awesome {\bf book} with an excellent open-source {\bf ray
      tracer}
    \url{https://pbrt.org}
  \item Great example of {\tt Donald Knuth}'s literate
    programming~\footnote{Literate programming:\\
      \quad \qquad \url{https://en.wikipedia.org/wiki/Literate_programming}\\
      \quad \qquad \url{http://www.literateprogramming.com}}
  \item Three editions and now {\bf free available} on line:
    \begin{figure}
      \centering
      \begin{subfigure}[b]{0.2\textwidth}
        \includegraphics[width=\textwidth]{images/pbrt1}
        \caption*{2004: 1st Ed.}
      \end{subfigure}
      \begin{subfigure}[b]{0.2\textwidth}
        \includegraphics[width=\textwidth]{images/pbrt2}
        \caption*{2010: 2nd Ed.}
      \end{subfigure}
      \begin{subfigure}[b]{0.2\textwidth}
        \includegraphics[width=\textwidth]{images/pbrt3}
        \caption*{2016: 3rd Ed.}
      \end{subfigure}
    \end{figure}
    \begin{itemize}
    \item Online free edition (October 15, 2018): \url{http://www.pbr-book.org}
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Physically Based Rendering}
  \framesubtitle{From Theory to \underline{Implementation}}

  \begin{block}{PBRT: the engine}
    \begin{itemize}
    \item {\bf PBRT}: Free and open source {\bf ray tracer} from the book\\
      {\it Physically Based Rendering: From Theory to Implementation}\\
      {\small \url{https://pbrt.org} \hfill
        \url{https://github.com/mmp/pbrt-v3}}
      \begin{itemize}
      \item Advance and complete physically-based ray tracing engine
      \item Focuses on academic use (literate programming!)
      \item Fork focused on artistic use:
          \begin{itemize}
          \item[] {\it LuxCoreRender} (formerly known as LuxRender)
          \item[] {\small \url{https://luxcorerender.org}}
          \item[] {\small \url{https://github.com/LuxCoreRender}}
          \end{itemize}
      \end{itemize}
    \end{itemize}
  \end{block}

  \begin{figure}
    \includegraphics[width=0.33\textwidth]{images/San_Pedro_01b.jpg}
    \includegraphics[width=0.33\textwidth]{images/cuisine-LGX2_b.jpg}
    \caption{Examples from \href{https://luxcorerender.org/gallery}{LuxCoreRender's Gallery}.}
  \end{figure}
}

\subsection{Main design aspects}

\subsubsection{Cameras}

\frame{
  \frametitle{\insertsubsubsection}
  \framesubtitle{\insertsubsection}

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics{images/pinhole_camera}
      \caption{Basic idea: pinhole camera}
    \end{figure}
    \onslide<2>
    \begin{figure}
      \includegraphics[width=0.5\textwidth]{images/pinhole_camera}
    \end{figure}

    \begin{figure}
      \includegraphics[width=0.7\textwidth]{images/simulated_pinhole_camera}
      \caption{Simulated pinhole camera: film plane in front of the
        pinhole, same distance}
    \end{figure}
    \onslide<3>
    \begin{figure}
      \includegraphics[width=0.5\textwidth]{images/pinhole_camera}
    \end{figure}

    \begin{figure}
      \includegraphics[width=0.7\textwidth]{images/viewfrustum}
      \caption{Viewing volume (aka {\it view frustum)}}
    \end{figure}
    \onslide<4>
    \vspace*{-0.2cm}
    \begin{figure}
      \includegraphics[width=0.7\textwidth]{images/hitheryon}
    \end{figure}

    \begin{itemize}
    \item What color value do we display at each point in the image?\\[-0.1cm]
      \begin{itemize}
      \item Amount of light traveling from the image point (film) to the eye
      \end{itemize}
    \end{itemize}
    \begin{block}{Task of the camera simulator (encapsulation!)}
      Take a {\bf point on the image} and generate {\bf rays} along which light is
      known to contribute to that image location.
    \end{block}
  \end{overprint}
}

\subsubsection{Ray-object intersections}

\frame{
  \frametitle{\insertsubsubsection}
  \framesubtitle{\insertsubsection}

  \begin{itemize}
  \item First task for each generated ray: determine {\bf which object} (if
    any) that ray intersects \underline{first} and {\bf where} the
    intersection occurs.
    \begin{itemize}
    \item \structure{Objective}: simulate interaction of light with
      the object at this point.
    \end{itemize}

    \pause

  \item A ray $\mathrm{r}$ is expressed in {\it parametric form}:
    $$\mathrm{r} (t) = \mathrm{o} + t \vec{\mathrm{\bf d}}$$

    \pause

  \item Easy to find intersection between the ray $\mathrm{r}$ and a
    surface defined by an implicit function  $\mathrm{F} (x,y,z) = 0$

    \pause

  \item Naive way to find closest intersection: {\bf brute-force}
    approach, testing the ray against \underline{each object} in the scene in
    turn, choosing the minimum $t$ value of the intersections found.

    \pause

  \item Efficient solution for complex scenes: {\bf acceleration
      struct.}
    \begin{itemize}
    \item quickly rejects whole groups of objects during the ray intersection process.
    \item e.g. $O(I \ N) \Rightarrow O(I \log N)$\\
      {\footnotesize [for $I$ pixels to compute and $N$ objects in the scene]}
    \item Spatial subdivision (BSP, KD-Tree, etc.), Bounding volume
      hierarchies\ldots
    \end{itemize}

  \end{itemize}
}

\begin{frame}[t]
  \frametitle{\insertsubsubsection~(cont.)}
  \framesubtitle{\insertsubsection}

  \begin{itemize}
  \item First task for each generated ray: determine {\bf which object} (if
    any) that ray intersects \underline{first} and {\bf where} the
    intersection occurs.
    \begin{itemize}
    \item \structure{Objective}: simulate interaction of light with
      the object at this point.
    \end{itemize}

  \item {\bf \ldots}

  \item Finally: knowing intersection point is not enough!

    Ray tracer also needs to know certain {\bf properties of the
      surface} \underline{at the point}.
    \begin{itemize}
    \item Representation of the {\bf material} at that point is
      obtained and passed along to later stages of the ray-tracing
      algorithm
    \item {\bf Additional geometric} info needed to shade the point:
      eg. normal vector
    \end{itemize}
  \end{itemize}

\end{frame}

\subsubsection{Light sources \& light distribution}

\frame{
  \frametitle{\insertsubsubsection}
  \framesubtitle{\insertsubsection}

  \begin{itemize}
  \item Ray-object intersection give us a point to be shaded and some
    information about the geometry at that point.
  \item Recall \structure{eventual goal}: find the amount of light
    leaving this point in the direction of the camera.

    \pause

  \item So, we need to know how much light is \underline{arriving} at
    this point
    \begin{itemize}
    \item involves both {\bf geometric} and {\bf radiometric}
      distribution of light in the scene
    \end{itemize}

    \pause

  \item Naive (unrealistic) lighting: point lights
    \begin{itemize}
    \item Power $\Phi$ associated
    \item Radiates light equally in all directions ($4\pi$)
    \item Energy arriving at a point on a sphere of radius $r$
      proportional to $\frac{1}{r^2}$
    \item Differential irradiance deposited on a surface patch $dA$
      (angle $\theta$ with light):
      $$dE = \frac{\Phi \cos \theta}{4 \pi r^2}$$
    \end{itemize}
  \end{itemize}

  \begin{overprint}
    \onslide<3>
    \vspace*{-0.5cm}
    \begin{figure}
      \includegraphics{images/lightdistro} \qquad
      \includegraphics[width=0.2\textwidth]{images/lightdistro2}
    \end{figure}
    \onslide<4>
    \begin{itemize}
    \item Easy handle multiple lights: illumination is linear
      \begin{itemize}
      \item contributions of each light can be computed separately and
        summed to obtain the overall contribution
      \end{itemize}
    \end{itemize}
  \end{overprint}
}

\subsubsection{Visibility}

\frame{
  \frametitle{\insertsubsubsection}
  \framesubtitle{\insertsubsection}

  \begin{itemize}
  \item Essential component to add to lighting distribution: shadows.
  \item Each light contributes illumination to the point being shaded
    only if the path between them is unobstructed.
  \item Visibility is easy to determine in a ray tracer:
    \begin{description}
    \item[Shadow ray:] new ray whose origin is at the surface point and
      whose direction points toward the light.
      \begin{itemize}
      \item Simple parametric $t$ comparison
      \end{itemize}
    \end{description}
  \end{itemize}

  \begin{figure}
    \begin{overprint}
      \onslide<1>
      \centering
      \includegraphics[width=0.5\textwidth]{images/oclusion0}
      \onslide<2>
      \centering
      \includegraphics[width=0.5\textwidth]{images/oclusion}
    \end{overprint}
    \caption{Occlusion and visibility}
  \end{figure}
}

\subsubsection{Surface scattering}

\begin{frame}[fragile]
  \frametitle{\insertsubsubsection}
  \framesubtitle{\insertsubsection}

  \begin{itemize}
  \item So far: two pieces of information for proper shading of a
    point
    \begin{itemize}
    \item its location
    \item incident lighting (at least direct lighting)
    \end{itemize}

  \item Next step: how incident light is {\bf scattered} at the surface
    \begin{itemize}
    \item specifically, how much light energy is scattered back along
      the ray originally traced to find intersection point (leads to camera)
    \end{itemize}

  \item Material/appearance prop. $\Rightarrow$
    {\it Bidirectional Reflectance Distribution Function}
    \begin{description}
    \item[BRDF] tells how much energy is reflected from a given
      incoming direction $\omega_i$ to a given outgoing dir. $\omega_o$
    \end{description}
  \end{itemize}
  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=0.5\textwidth]{images/surfacescat}
      \caption{BRDF at p: $f_r(\mathrm{p}, \omega_o, \omega_i)$}
    \end{figure}
    \onslide<2>

    \hspace*{-0.8cm}\fcolorbox{blue}{blue!20}{
    \hspace*{-0.4cm}\begin{minipage}{1.15\textwidth}
    \begin{algorithmic}
      \ForAll{light in scene}

        \If{light is not blocked}

          \State $incident\_light \gets \Call{light.L}{hit\_point}$

          \State $amount\_reflected \gets
          \Call{surface.BRDF}{hit\_point, camera\_vector, light\_vector}$

          \State $L += amount\_reflected \times incident\_light$

        \EndIf
      \EndFor
    \end{algorithmic}
    \end{minipage}
}

    \onslide<3>
    \begin{itemize}
    \item BRDF can be generalized to {\bf transmitted} light (BTDF) or to
      general scattering of light arriving from either side of the
      surface:
      \begin{center}
        {\it Bidirectional Scattering Distribution Function}
        (BSDF)
      \end{center}
    \end{itemize}

    \onslide<4>
    \begin{itemize}
    \item BRDF can be generalized to {\bf transmitted} light (BTDF) or to
      general scattering of light arriving from either side of the
      surface:
      \begin{center}
        {\it Bidirectional Scattering Distribution Function}
        (BSDF)
      \end{center}

    \item Or even to...
      \begin{center}
        {\it Bidirectional Scattering Surface Reflectance Distribution Function}
        (BSSRDF)
      \end{center}
      which models light that exits a surface at a different point than it enters.
    \end{itemize}
  \end{overprint}
\end{frame}

\subsubsection{Indirect light transport}

\frame{
  \frametitle{\insertsubsubsection~(recursive ray tracing)}
  \framesubtitle{\insertsubsection}

  \begin{itemize}
  \item Turner Whitted's original paper on ray tracing {\bf (1979)}
    emphasized its {\it recursive} nature
    \begin{itemize}
    \item Typical effects in ray tracing engines: mirrors, glasses and
      glossy surfaces
    \end{itemize}
  \end{itemize}
  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=0.65\textwidth]{images/blender-glass.png}
    \end{figure}
    \onslide<2>
    \begin{figure}
      \includegraphics[width=0.85\textwidth]{images/ballsrender.png}
    \end{figure}
    \onslide<3>
    \begin{figure}
      \includegraphics[width=0.5\textwidth]{images/recursiontree}
    \end{figure}
    \onslide<4->
    \begin{itemize}
    \item Amount of light reaching the camera from a point on an
      object is given by the sum of light emitted by the object and
      the amount of reflected light:
      \begin{center}
        {\bf Light transport equation} ({\it rendering equation},
        James Kajija 1986)
      \end{center}
      $$L_o(p,\omega_o) = L_e(p,\omega_o) + \int_{S^2} f_r(\mathrm{p},
      \omega_o, \omega_i) L_i(p,\omega_i) |\cos \theta_i|
      \operatorname{d}\omega_i$$

      \vspace*{-0.2cm}
      \begin{itemize}
      \item<5-> \underline{Classical Whitted's algorithm simplifies computation}:
        only incoming energy from {\bf light sources} and perfect
        {\bf reflection} and {\bf refraction}, ignoring incoming
        light from most directions.\\[0.2cm]
      \item<6> However, easily extensible to capture more effects or
        solve rendering equation
        \begin{itemize}
        \item glossy surfaces
        \item lots of rays and recursion (tree of rays): \structure{Monte Carlo!}
        \end{itemize}
      \end{itemize}
    \end{itemize}
  \end{overprint}
}

\subsubsection{Ray propagation}

\frame{
  \frametitle{\insertsubsubsection}
  \framesubtitle{\insertsubsection}

  \begin{itemize}
  \item Do {\bf not} assume that rays (energy) are travelling through
    a vacuum.
  \item Participating media: smoke, fog, dust, Earth's atmosphere\ldots
  \item 2 ways in which a participating medium affects light
    propagating along a ray:
    \begin{itemize}
    \item Attenuation: medium extinguish light, either bu absorbing it
      or by scattering it in a different direction (transmittance $T$)
    \item A participating medium can also add to the light along a ray
      (evaluate {\it volume light transport equation})
    \end{itemize}
  \end{itemize}
}

\subsection{An RT engine}

\frame{
  \frametitle{A Ray Tracing engine}
  \framesubtitle{Usual approach: object-oriented design}

  \vspace*{-0.5cm}
  \begin{figure}
    \hspace*{-1.15cm}
    \includegraphics[width=1.2\textwidth]{images/BDE_final_class_dia.pdf}
    \caption{Example of a \underline{point-based} ray tracer {\footnotesize
        (BDE, Omar \'Alvarez's PFC@FIC)}.}
  \end{figure}
}

\frame{
  \frametitle{A Ray Tracing engine}
  \framesubtitle{PBRT Engine}

  \begin{itemize}
  \item Core in {\tt src/core/}
  \item Most of {\bf pbrt} implemented in terms of 10 key abstract base classes
  \end{itemize}
  \begin{table}
    \begin{tabular}{ll}
      \toprule
          {\bf Base class} & {\bf Directory}\\\midrule
          Shape & shapes/ \\
          Aggregate & accelerators/ \\
          Camera & cameras/ \\
          Sampler & samplers/ \\
          Filter & filters/ \\
          Film & filters/ \\
          Material & materials/ \\
          Texture & textures/ \\
          Medium & media/ \\
          Light & lights/ \\
          Integrator & integrators/\\\bottomrule
    \end{tabular}
  \end{table}
}

\frame{
  \frametitle{A Ray Tracing engine}
  \framesubtitle{PBRT Engine}

  \begin{itemize}
  \item Two phases of execution:
    \begin{enumerate}
    \item Parsing: scene description and rendering params
      \begin{itemize}
      \item Results in instances of {\tt Scene} and {\tt Integrator} classes
      \end{itemize}
    \item Main rendering loop
      \begin{itemize}
      \item Implemented in an implementation of the {\tt
        Renderer::Render()} method
      \item Decomposed in RendererTasks since pbrt v2 to get parallelism
        ({\it screen-space decomposition})
      \end{itemize}
    \end{enumerate}

    \begin{figure}
      \includegraphics[width=0.3\textwidth]{images/furrydog}
      \caption{Example from the book}
    \end{figure}
  \end{itemize}
}

\frame{
  \frametitle{A Ray Tracing engine}
  \framesubtitle{Most of the render time\ldots}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/diagram}
    \caption{Main rendering loop (pbrt\footnote{pbrt: {\it Physically-based
      Rendering: From Theory to Implementation}, by Matt Pharr and Greg Humphreys.})}
  \end{figure}
}

\frame{
  \frametitle{A Ray Tracing engine}
  \framesubtitle{Class relationships for SurfaceIntegration}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/surfaceintegration}
    \caption{WhittedIntegrator class (pbrt)}
  \end{figure}
}


\section{Bibliography}

\frame{
  \frametitle{References}
  \framesubtitle{Physically-based Rendering}

  \beamertemplatebookbibitems
  \nocite{*}
  \printbibliography[keyword=PBR]
}

\frame{
\thispagestyle{empty}

\begin{center}

  Thank you very much for your attention!\\[0.5cm]
  All questions and comments are welcome :-)
\ \\ \ \\ \ \\

\includegraphics[width=0.5\textwidth]{images/cgf_cover.jpg}\\

%\ \\ \ \\ \ \\ \ \\
\textbf{Physically-based Rendering (PBR)}\\
Offline rendering of photo-realistic synthetic images
\ \\ \ \\ \ \\% \ \\

\begin{scriptsize}
\textbf{Emilio Jos\'e Padr\'on Gonz\'alez}\\emilioj@udc.gal\\
MUEI, IGM 2018/19
\end{scriptsize}
\end{center}
}

\end{document}

\section{Class \& Home work}

\frame{
  \frametitle{Class work}
  \framesubtitle{Tasks}

  \begin{enumerate}
  \item Download {\it pbrt v2} code
    \begin{itemize}
    \item Recommended: {\tt git clone git://github.com/mmp/pbrt-v2.git}
    \end{itemize}
  \item Compile and run the program
    \begin{itemize}
    \item {\tt pbrt -{-}help} to know how to execute it
    \item Render some of the example scenes
    \end{itemize}
  \end{enumerate}

  \begin{figure}
    \centering
    \includegraphics[width=0.45\textwidth]{images/killeroo-simple.png}
    \vspace*{-0.2cm}\caption{{\it Killeroo-simple} scene.}
  \end{figure}
}

\frame{
  \frametitle{Class work}
  \framesubtitle{Tasks (cont.)}

  \begin{enumerate}
    \setcounter{enumi}{2}

  \item A good way to gain an understanding of pbrg is to follow the
    process of computing the radiance value for a single ray using a
    debugger (e.g. GNU GDB).

    \begin{itemize}
    \item Build a version of pbrt with debugging symbols and set up your
      debugger to run pbrt with the {\it killerloo-simple.pbrt} scene
      from the scenes directory.
    \item Set breakpoints in the
      {\tt SamplerRenderer::Render()} and {\tt
        SamplerRendererTask::Run()} functions and trace through the
      processes.
    \item The aim of this task is getting familiar with the
      software by observing how a ray is generated, {\bf how its radiance
      value is computed}, and {\bf how its contribution is added to the
      image}.
    \item You may want to specify that
      only a single thread of execution should be used by providing
      {\tt {-}{-}ncores 1} as command-line arguments to pbrt; doing so ensures that
      all computation is done in the main process thread, which may make
      it easier to understand what is going on, depending on how easy
      your debugger makes it to step through the program when it is
      running multiple threads.
    \end{itemize}
  \end{enumerate}
}


\frame{
  \frametitle{Class/home work}
  \framesubtitle{Tasks (cont.)}

  \begin{enumerate}
    \setcounter{enumi}{3}
  \item \structure{TODO (groups of two students)}\\ Create these two deliverable documents:
    \begin{description}
    \item[Doc1: Perf. Analysis.] Comparative performance report
      of the \underline{3 acceleration structures} implemented in pbrt:
      \texttt{\it Kd-tree}, \texttt{\it BVH}, \texttt{\it Grid}
      \begin{itemize}
      \item use different test scenes (at least 3)
      \item test each accelerator with different configurations:
        depth, partition decision, heuristics\ldots whatever.
      \item performance metrics: {\bf time} \& {\bf mem}.
      \item both {\bf construction time} and {\bf rendering time} must
        be reflected in the analysis.
      \item do NOT forget to describe the computing platform and the
        different parameters used in the executions.
      \item include some render! (\ldots and comment about possible
        noticeable differences between accel.)
      \end{itemize}
    \item[Doc2: Tracing execution.] A diagram (text and/or figures)
      describing how the radiance value for a single ray is computed,
      as you have it interpreted from your 'debugging' task: main
      steps, involved classes\ldots
      % \item DEADLINE: Wed Jan 17, 2013 (email to {\tt emilioj@udc.gal})
      % \item A brief defense (about 15 minutes) of each group's work
      %   will be needed to get the exercise passed: Thu18 or
      %   Fri19. Each group must arrange a meeting (again, {\tt
      %     emilioj@udc.gal}) to do it.
      % \end{itemize}
    \end{description}
  \end{enumerate}
}

\end{document}
