\documentclass[serif, 10pt, gray, compress]{beamer}

\usepackage{myBeamerStyle}
\setbeamertemplate{itemize subitem}[default]
\setbeamertemplate{itemize subsubitem}[balls]
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usepackage{minted}

\usepackage[sorting=none, backend=bibtex, defernumbers=true]{biblatex}
\addbibresource{bibliography.bib}

\title{MUEI - IGM 2018/19}
\author{RT Rendering with OpenGL}
\date{}

\begin{document}

\frame{
\thispagestyle{empty}

\begin{center}

\ \\% \ \\ \ \\

\includegraphics[width=250px]{images/doom4k}\\

%\ \\ \ \\ \ \\ \ \\
\textbf{Real-Time Rendering}\\
with OpenGL
\ \\ \ \\ \ \\% \ \\ \ \\

\begin{scriptsize}
\textbf{Emilio Jos\'e Padr\'on Gonz\'alez}\\emilioj@udc.gal\\ \ \\
MUEI, IGM 2018/19
\end{scriptsize}
\end{center}
}

\AtBeginSection[] {
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection, hideothersubsections]
  \end{frame}
}

\begin{frame}<beamer>
  \frametitle{Outline}
  \tableofcontents[hideothersubsections]
\end{frame}


\section{Introduction}

\frame{
  \frametitle{Interactive Computer Graphics}

  \begin{block}{Real-Time Rendering}
    \begin{itemize}
    \item Interactivity
    \item Speed matters
      \begin{itemize}
      \item at least 6 frames per second (FPS)
      \item tipically, a minimum of 18-20 FPS
      \item ideally, between 30-60 FPS
        \begin{itemize}
        \item[$\Rightarrow$] {\it budget} for getting a new frame: 33.3ms or 16.6ms
        \end{itemize}
      \end{itemize}
    \item Very optimized software \& hardware
    \end{itemize}
  \end{block}
}

\subsection{OpenGL: evolution \& current status}

\frame{
  \frametitle{OpenGL}

  \begin{block}{OpenGL (Open Graphics Library)}
    Cross-language, multi-platform application programming interface
    (\structure{API}) for rendering 2D and 3D computer graphics.
    \begin{description}
      \item[Main Goal:] provide an abstraction layer between your
        application and the underlying graphics subsystem
    \end{description}
  \end{block}

  \begin{itemize}
  \item Typically used to interact with a Graphics Processing
    Unit (GPU), to achieve hardware-accelerated \structure{real-time
      rendering}
  \item Hardware agnostic/independent
  \item Purely concerned with rendering, providing no APIs related to
    input, audio, or windowing
  \end{itemize}

  \begin{overprint}

    \onslide<1>
    \begin{figure}
      \includegraphics[width=.5\textwidth]{images/opengl_logo}
    \end{figure}

    \onslide<2>

    \vspace*{-.5cm}
    \begin{block}{}
      \begin{itemize}
      \item[\checkmark] Developed by Silicon Graphics Inc. (SGI): 1st release in 1992
      \item[\checkmark] Currently managed by the non-profit technology consortium
        {\bf Khronos Group}
      \item[\checkmark] Next Generation: \structure{Vulkan}
        \begin{itemize}
        \item lower overhead, more direct control over the GPU, and lower CPU usage
        \end{itemize}
      \end{itemize}
    \end{block}
  \end{overprint}
}

\frame{
  \frametitle{OpenGL}
  \framesubtitle{Versions and free implementation}

  \begin{block}{Current API version}
    \begin{itemize}
    \item OpenGL 4.6 {\small \it (July 2017)}
    \item GLSL (OpenGL Shading Language) 4.60
    \item OpenGL ES (OpenGL for Embedded Systems) 3.2 {\small \it (August 2015)}
    \end{itemize}
  \end{block}

  \begin{table}\footnotesize
    \hspace*{-0.6cm}
    \begin{tabular}{l@{\hspace{0.2cm}}l@{\hspace{0.3cm}}l@{\hspace{0.3cm}}l@{\hspace{0.35cm}}l@{\hspace{0.35cm}}l@{\hspace{0.35cm}}l@{\hspace{0.35cm}}l@{\hspace{0.35cm}}l@{\hspace{0.35cm}}l@{\hspace{0.35cm}}l@{\hspace{0.35cm}}l@{\hspace{0.45cm}}l}
      \toprule
      OpenGL & 2.0 & 2.1 & 3.0 & 3.1 & 3.2 & 3.3 & 4.0 & 4.1 & 4.2 & 4.3
      & 4.4 & 4.5\\
      GLSL & 1.10 & 1.20 & 1.30 & 1.40 & 1.50 & 3.30 & 4.00 & 4.10 &
      4.20 & 4.30 & 4.40 & 4.50\\\midrule
      Mesa & & 7.0 & 8.0 & 9.0 & & 10.0 & & 11.0 & & 12.0 & 13.0 & \hspace*{-0.3cm}$17.2$-$18.2$\\\bottomrule
    \end{tabular}
  \end{table}

  \setbeamersize{description width=0.6cm}
  \begin{description}\small
  \item[Mesa] Collection of free and open-source libraries that implement OpenGL and several other APIs related to hardware-accelerated 3D rendering, 3D computer graphics and GPGPU. {\footnotesize \hfil \hfil \url{http://www.mesa3d.org}}\\
    \begin{minipage}{\linewidth}
    \begin{block}{}
    {\footnotesize \structure{Current version:} Mesa 18.3 (December
      2018)}\\
    {\footnotesize $\rightarrow$Full support of OpenGL 4.6 is not ready}\\
    {\footnotesize \alert{OpenGL API coverage}: \url{http://mesamatrix.net}}
    \end{block}
    \end{minipage}
  \end{description}
}

\frame{
  \frametitle{OpenGL}
  \framesubtitle{The Mesa 3D Graphics Library}

  \begin{block}{Mesa 3D}
    \begin{itemize}
    \item Open source software implementation of OpenGL, Vulkan, and
      other graphics API specifications.
    \item Mesa translates these specifications to vendor-specific
      graphics hardware drivers.
    \end{itemize}
  \end{block}

  \begin{itemize}
  \item[--] hosted by {\bf freedesktop.org} and initiated in 1993 by Brian
    Paul as a personal project.
  \item[--] widely adopted, present in all GNU/Linux distros.
  \item[--] mainly C, with some C++ and Python.
  \end{itemize}

  \begin{overprint}

    \vspace*{-0.35cm}

    \onslide<2>

    \begin{exampleblock}{Hardware Drivers}
      \begin{columns}
        \column{.47\textwidth}
        \begin{itemize}
        \item[] Intel (i965, i915, anvil)
        \item[] AMD (radeonsi, r600, radv)
        \item[] NVIDIA (nouveau)
        \item[] Imagination (imx)
        \end{itemize}
        \column{.45\textwidth}
        \begin{itemize}
        \item[] Broadcom (vc4, vc5)
        \item[] Qualcomm (freedreno)
        \item[] Freescale (etnaviv)
        \end{itemize}
      \end{columns}
    \end{exampleblock}

    \onslide<3>

    \begin{exampleblock}{Corporations contributing}
      \begin{columns}[t]
        \column{.3\textwidth}
        \vspace{-0.2cm}
        \begin{itemize}
        \item AMD
        \item Broadcom
        \item Collabora
        \item Feral Interactive
        \end{itemize}
        \column{.3\textwidth}
        \vspace{-0.2cm}
        \begin{itemize}
        \item Google
        \item Igalia
        \item Intel
        \item NVIDIA
        \end{itemize}
        \column{.3\textwidth}
        \vspace{-0.2cm}
        \begin{itemize}
        \item Pengutronix
        \item RedHat
        \item Samsung
        \item Valve
        \item VMware
        \end{itemize}
      \end{columns}
    \end{exampleblock}
  \end{overprint}
}

\frame{
  \frametitle{OpenGL}
  \framesubtitle{Useful additional libraries and tools}

  \begin{itemize}
  \item System-level I/O with the host operating system: window
    definition with an OpenGL context, window control, monitoring of
    keyboard and mouse input\ldots
    \begin{itemize}
    \item \structure{GLUT} (OpenGL Utility Toolkit) \hfil \alert{*legacy*}
    \item \structure{FreeGLUT} \hfil \hfil \hfil~~~~~\alert{*legacy*}
    \item \structure{GLFW}: the modern (simple) approach
      \begin{itemize}
      \item[$\rightarrow$] Bigger libraries such as \structure{SDL} or
        \structure{SFML} can also be used
      \end{itemize}
    \end{itemize} \vspace*{0.2cm}
  \item Managing OpenGL extensions on the target platform
    \begin{itemize}
    \item \structure{GLEW} (OpenGL Extension Wrangler Library)
    \end{itemize} \vspace*{0.2cm}
  \item Higher-level drawing routines
    \begin{itemize}
    \item \structure{GLU} (OpenGL Utility Library) \hfil \alert{*legacy*}
      \begin{itemize}
      \item[] \hspace*{-0.75cm} $\rightarrow$Modern version (GLSL-based): \structure{GLU} (Modern OpenGL Utility Library)\\
        \hspace*{-0.45cm} \url{http://glu.g-truc.net}
      \end{itemize}
    \end{itemize} \vspace*{0.2cm}
  \item Mathematics library for graphics software based on GLSL
    \begin{itemize}
    \item \structure{GLM} (OpenGL Mathematics)\\
      \url{http://glm.g-truc.net}
    \end{itemize} \vspace*{0.2cm}
  \item Image library for graphics software based on GLSL
    \begin{itemize}
    \item \structure{GLI} (OpenGL Image)\\
      \url{http://gli.g-truc.net}
    \end{itemize}
  \end{itemize}
}



\section{3D Rendering Pipeline}


\frame{
  \frametitle{The Graphics Pipeline}

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=\linewidth]{images/pipeline_coarse}
      \caption{Basic 3D Rendering Pipeline}
    \end{figure}

    \onslide<2>
    \begin{figure}
      \includegraphics[width=0.5\linewidth]{images/pipeline_coarse}
    \end{figure}

    \begin{figure}
      \hspace*{-0.5cm}\includegraphics[width=1.1\linewidth]{images/pipeline_geometry}
      \caption{Tasks in the Geometry Stage}
    \end{figure}
  \end{overprint}
}


\begin{frame}{Graphics pipeline: basic stages}

  In a simplified way, a modern graphics pipeline can be depicted as
  these steps:
  \begin{center}
    Vertex Shader\\
    $\downarrow$\\
    Tessellation\\
    $\downarrow$\\
    Geometry Shader\\
    $\downarrow$\\
    Rasterization\\
    $\downarrow$\\
    Fragment Shader
  \end{center}

\end{frame}

\frame{
  \frametitle{Modern OpenGL Pipeline}

  \begin{figure}
    \centering
     \includegraphics[height=0.9\textheight]{images/RenderingPipeline}
    \caption{OpenGL Rendering Pipeline}
  \end{figure}
}

\frame{
  \frametitle{OpenGL 4.x Pipeline}
  \framesubtitle{Primitives, Pipelines and Pixels}

  \begin{itemize}
  \item OpenGL~\footnote{Good online OpenGL tutorial:
      \href{https://learnopengl.com}{Learn OpenGL} --
      \url{https://learnopengl.com}} model: pipeline with a one-way
    data flow
    \begin{itemize}
    \item Graphics data from application enters front of pipeline
    \item Flows from stage to stage until reaches end of pipeline
    \end{itemize}
  \item Fundamental \underline{unit of rendering}: the {\it primitive}
    \begin{itemize}
    \item 3 basic renderable primitive types: {\bf points}, {\bf
      lines} and {\bf triangles}
    \item Formed from collections of one, two or three {\it vertices}
      \begin{itemize}
      \item[$\Rightarrow$] {\bf vertex:} a point within a coordinate space
      \end{itemize}
    \end{itemize}
  \end{itemize}

  \begin{figure}
    \centering
    \hspace*{-0.5cm}\includegraphics[width=1.1\linewidth]{images/gl-pipeline.png}
    \caption{OpenGL Rendering Pipeline}
  \end{figure}
}


\subsection{Basic Shader Plumbing in OpenGL}

\frame{
  \frametitle{CPU and GPU work together to get each new frame}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/basicpiping}
  \end{figure}

  \begin{itemize}
  \item CPU prepares data and issues commands for the GPU to process
    those data and generate a new frame
    \begin{itemize}
    \item RAM (CPU) $\rightarrow$ VRAM (GPU) data transfer (PCIe
      BUS)
    \item CPU request GPU to draw things by asserting {\bf draw
        calls} \vspace*{-0.3cm}\begin{block}{}\centering
        \parbox{3.2cm}{Application (e.g. game engine's rendering code)}
        $\rightarrow$ Graphics API $\rightarrow$ GPU Driver
        $\rightarrow$ GPU code
      \end{block}
    \end{itemize}\vspace*{0.2cm}
  \item When this is ready, GPU can start doing its job
  \end{itemize}
}

\frame{
  \frametitle{Basic Shader Plumbing}
  \framesubtitle{Piping data throughout the pipeline}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/basicpiping}
  \end{figure}

  \begin{overprint}

    \onslide<1>

    \begin{itemize}
    \item {\bf Shaders execution}: concurrent data flow!
      \begin{itemize}
      \item Vertex shader runs once per input vertex
      \item Fragment shader runs once per generated fragment
      \end{itemize}
    \end{itemize}

    \onslide<2>
    \begin{itemize}
    \item {\bf Shader stage inputs and outputs}:
      \begin{description}
      \item[User defined:] 'pipes' connecting the different
        stages
        \begin{itemize}
        \item[\footnotesize Eg.] {\footnotesize {\tt vs\_output} in
            the figure above is connecting vertex and fragment
            shaders.}
        \end{itemize}

      \item[Built-in:] communication with certain fixed-functionality
        \begin{itemize}
        \item[\footnotesize Eg.] {\footnotesize {\tt gl\_Position} is
            a built-in output variable in vertex shader to set the
            clip-space output position of the current vertex.}
        \item[\footnotesize Eg.] {\footnotesize {\tt gl\_VertexID} is a built-in
            input variable in vertex shader with the index of the
            vertex being processed.}
        \end{itemize}
      \end{description}
    \end{itemize}

    \onslide<3>
    \begin{itemize}
    \item {\bf Vertex Attributes}: feeding the vertex shader
      \begin{itemize}
      \item Input variables to the vertex shader $\Rightarrow$ How
        vertex data is introduced into OpenGL pipeline
        \begin{itemize}
        \item[in] \emph{datatype} {\bf varname}
        \end{itemize}
      \item Data in {\it Buffer Objects} (previously allocated in GPU
        memory) are connected to Vertex Shader's user-defined input
        variables.
      \item Every Vertex Shader invocation gets new data in its
        input variables from the vertex attributes.
      \item How to ``fill'' this variables?
        \begin{description}[\hspace*{-0.5cm}]
        \item[Vertex~attribute~functions:] {\tt glVertexAttrib*()}\\
          {\footnotesize e.g. {\tt void glVertexAttrib4fv(GLuint index, const GLfloat *v)}}
        \end{description}
      \end{itemize}
    \end{itemize}

    \onslide<4>
    \begin{itemize}
    \item {\bf Vertex Attributes}: feeding the vertex shader
      \begin{itemize}
      \item Vertex attribute's index:
        \begin{itemize}
        \item Implicit: position, i.e. definition order
        \item Explicit: \emph{layout qualifier}\\
          {\tt layout (location = 0) in \emph{datatype} {\bf varname}}
        \end{itemize}
      \end{itemize}
    \end{itemize}

    \onslide<5>
    \setbeamertemplate{itemize item}{\tiny\raise1.5pt\hbox{\donotcoloroutermaths$\blacktriangleright$}}

    {\footnotesize
    \begin{block}{Buffer Objects management}
      \begin{itemize}
      \item Basic routines: {\scriptsize {\tt glGenBuffers}, {\tt glBindBuffer}, {\tt glBufferData}}
      \item Additionaly: {\scriptsize {\tt glBufferSubData}, {\tt glMapBuffer}\ldots}
      \end{itemize}
    \end{block}
    }

    {\footnotesize
    \begin{block}{Vertex Attributes management}
      \begin{itemize}
      \item Vertex shader attribute \underline{index}: implicit or explicit with \emph{layout qualifier}
        {\scriptsize \tt layout(location = attribute index) in type vs\_input;}
      \item Basic routines: {\scriptsize {\tt glVertexAttrib4fv}, {\tt
            glVertexAttribPointer}, {\tt glEnableVertexAttribArray}\ldots}
      \end{itemize}
    \end{block}
  }

  \end{overprint}

}

\frame[t]{
  \frametitle{\insertsubsectionhead}
  \framesubtitle{Uniform variables: parameters for shaders}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/basicpiping2}
  \end{figure}

  \begin{itemize}
  \item {\bf Uniforms}: passing data directly from application into any
    shader stage.
    \begin{itemize}
    \item Basically: state data constant during the draw call\\
      E.g. transformation matrices
    \end{itemize}
  \end{itemize}

  {\footnotesize
    \begin{block}{Uniforms}
      \begin{itemize}
      \item Basic routines: {\scriptsize {\tt glUniform*}, {\tt glUniformMatrix*} (transfer scalar, vector or matrix data)}
      \item Additionaly: {\scriptsize {\tt glGetUniformLocation}}
      \item Advanced: Uniform Blocks, using {\it buffer objects}
      \end{itemize}
    \end{block}
  }
}


\section{OpenGL Programming: the very basics}


\subsection{System requirements}

\frame{
  \frametitle{\insertsectionhead}
  \framesubtitle{Using C/C++ as programming language and \underline{Mesa} as
    OpenGL implementation}

  \begin{itemize}
  \item Basic requirements {\footnotesize (package names are from Debian and derivatives)}
    \begin{itemize}\footnotesize
    \item C/C++ compiler: {\tt gcc}/{\tt g++} (GNU)
    \item Mesa headers and libraries: {\tt libgl1-mesa-dev} ({\tt mesa-common-dev}\ldots)
    \item GLEW: {\tt libglew-dev}
    \item GLFW: {\tt libglfw3-dev}
    \end{itemize}
  \end{itemize}

  \begin{itemize}
  \item Other useful tools/resources
    \begin{itemize}\footnotesize
    \item Mesa 3D utils: {\tt mesa-utils}
    \item GLEW utils: {\tt glew-utils}
    \item OpenGL man pages: {\tt opengl-4-man-doc}
    \item Online reference and OpenGL API at
      \url{https://www.khronos.org/opengl/wiki} and
      \url{https://www.khronos.org/registry/OpenGL-Refpages/gl4}
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Testing your OpenGL Installation!}

  \begin{enumerate}
  \item Install all the minimum requirements
  \item Check your OpenGL version with Mesa utils:
    \begin{itemize}
    \item {\tt glxgears}
    \item {\tt glxheads}
    \item {\tt glxinfo}
    \end{itemize}
  \item Check your OpenGL development environment with this simple
    test program:\\
    \url{http://gac.udc.es/~emilioj/test.c}
    \begin{itemize}
    \item[] {\footnotesize (also available cloning this git repo:
        \url{https://git.fic.udc.es/docencia-igm/public.git})}\\[0.2cm]
    \item Compile it:\\
      {\tt gcc -o test test.c -lglfw -lGLEW -lGL}\\[0.2cm]
    \item Execute it:\\
      (natively, that is using graphics hardware)\\
      {\tt ./test}\\[0.2cm]
      (or using software rendering)\\
      {\tt LIBGL\_ALWAYS\_SOFTWARE=1 ./test}
    \end{itemize}
  \end{enumerate}
}


\subsection{Preliminary stuff: context and window}

\begin{frame}[fragile]
  \frametitle{Preliminary stuff}
  \framesubtitle{OpenGL context and window {\tiny \it (I/II)}}

  \vspace*{-0.2cm}
  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}
#include <GL/glew.h>
#include <GLFW/glfw3.h>

int main() {
  // start GL context and O/S window using the GLFW helper library
  if (!glfwInit()) {
    fprintf(stderr, "ERROR: could not start GLFW3\n");
    return 1;
  }
  //  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  //  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  //  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  //  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(640, 480, "Test!", NULL, NULL);
  if (!window) {
    fprintf(stderr, "ERROR: could not open window with GLFW3\n");
    glfwTerminate();
    return 1;
  }
  [...]
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Preliminary stuff}
  \framesubtitle{OpenGL context and window {\tiny \it (II/II)}}

  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}

  [...]
  GLFWwindow* window = glfwCreateWindow(640, 480, "Test!", NULL, NULL);
  if (!window) {
    fprintf(stderr, "ERROR: could not open window with GLFW3\n");
    glfwTerminate();
    return 1;
  }
  glfwMakeContextCurrent(window);

  // start GLEW extension handler
  // glewExperimental = GL_TRUE;
  glewInit();

  /* [...] Rest of application [...] */

  // close GL context and any other GLFW resources
  glfwTerminate();

  return 0;
}
  \end{minted}
\end{frame}


\subsection{The Rendering Loop}

\begin{frame}[fragile]
  \frametitle{\insertsubsectionhead}
  \framesubtitle{{\tt processInput} \& {\tt update} \& {\tt render}}

  \begin{minted}[fontsize=\small, baselinestretch=1.4]{c++}

// Previously: Code for initializing some stuff and
// dealing with windowing systems, OpenGL context,
// OpenGL extensions, and so on...(i.e. GLFW, GLEW, etc.)
//
// [...]

// Typical rendering loop
while(running) {
  processInput();
  while(isTimeForUpdate) {
    update();
  }
  render();
}
  \end{minted}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Example of minimum OpenGL program}
  \framesubtitle{Basic: simply clears the whole screen to red}

  \begin{minted}[fontsize=\small, baselinestretch=1.4]{c++}

// Example of function to be called within rendering loop
void render(void) {
  glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
}
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Example of minimum OpenGL program}
  \framesubtitle{Basic, but a bit more sofisticated}

  \begin{minted}[fontsize=\small, baselinestretch=1.4]{c++}

// Example of function to be called within rendering loop
void render(double currentTime) {
  glClearColor(sin(currentTime) * 0.5f + 0.5f,
               cos(currentTime) * 0.5f + 0.5f,
               0.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
}
  \end{minted}
\end{frame}


\subsection{A {\it Hello Triangle!} OpenGL Program}

\frame{
  \frametitle{\insertsubsectionhead}

  Get this:\\
  \url{http://gac.des.udc.es/~emilioj/hellotriangle.c}

  \begin{center}
    \begin{minipage}{0.75\textwidth}
    {\footnotesize (also available cloning this git repo:\\
      \url{https://git.fic.udc.es/docencia-igm/public.git})}
  \end{minipage}
  \end{center}
}


\begin{frame}[fragile]
  \frametitle{\insertsubsectionhead}
  \framesubtitle{1. Data to be injected into render pipeline}

  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}

// [...]
// Preliminary stuff:
//                    - OpenGL context and window
//                    - OpenGL generic settings: Depth_test...
// [...]

// Triangle to be rendered (final screen coords)
float points[] = {
  0.0f,  0.5f,  0.0f,
  0.5f, -0.5f,  0.0f,
  -0.5f, -0.5f,  0.0f
};

[...]
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\insertsubsectionhead}
  \framesubtitle{2. GPU buffers: {\tt Vertex Buffer Objects} (VBO) and {\tt Vertex Array Objects} (VAO)}

  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}

// [...]
// 1. Data to be injected into render pipeline
// [...]

// Vertex Array Object
GLuint vao = 0;
glGenVertexArrays(1, &vao);
glBindVertexArray(vao);

// Vertex Buffer Object
GLuint vbo = 0;
glGenBuffers(1, &vbo);
glBindBuffer(GL_ARRAY_BUFFER, vbo);
glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW);

// Vertex attribute 0 (vertex position): 3 floats from offset 0 in vbo
glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
glEnableVertexAttribArray(0);
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\insertsubsectionhead}
  \framesubtitle{3. Dealing with Shaders: {\it vertex} shader and {\it fragment} shader}

  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}

// [...]
// 2. GPU buffers: VBOs and VAOs}
// [...]

// Vertex Shader
const char* vertex_shader =
"#version 130\n"
"in vec3 vp;"
"void main() {"
  "  gl_Position = vec4(vp, 1.0);"
  "}";

// Fragment Shader
const char* fragment_shader =
"#version 130\n"
"out vec4 frag_colour;"
"void main() {"
  "  frag_colour = vec4(0.5, 0.0, 0.5, 1.0);"
  "}";
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\insertsubsectionhead}
  \framesubtitle{4. Compiling, attaching and linking shaders}

  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}

// [...]
// 3. Shaders definition (inline or reading external files)}
// [...]

// Shaders compilation
GLuint vs = glCreateShader(GL_VERTEX_SHADER);
glShaderSource(vs, 1, &vertex_shader, NULL);
glCompileShader(vs);

GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
glShaderSource(fs, 1, &fragment_shader, NULL);
glCompileShader(fs);

// Create program, attach shaders to it and link it
GLuint shader_programme = glCreateProgram();
glAttachShader(shader_programme, fs);
glAttachShader(shader_programme, vs);
glLinkProgram(shader_programme);
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\insertsubsectionhead}
  \framesubtitle{5. Render loop}

  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}

// [...]
// 4. Compiling, attaching and linking shaders}
// [...]

// Render loop
while(!glfwWindowShouldClose(window)) {
  // wipe the drawing surface clear
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgram(shader_programme);
  glBindVertexArray(vao);
  // draw points 0-3 from the currently bound VAO with current in-use shader
  glDrawArrays(GL_TRIANGLES, 0, 3);
  // update other events like input handling
  glfwPollEvents();
  // put the stuff we've been drawing onto the display
  glfwSwapBuffers(window);
}

// close GL context and any other GLFW resources
glfwTerminate();

  \end{minted}
\end{frame}


% \subsection{Experimenting}

% \begin{frame}[allowframebreaks=0.8,t]
%   \frametitle{\insertsubsectionhead}
%   \begin{enumerate}
%   \item Load the shader strings from text files called {\tt
%     test\_vs.glsl} and {\tt test\_fs.glsl} (a naming convention is
%     handy).
%   \item Change the colour of the triangle in the fragment shader.
%   \item Try to move the shape in the vertex shader e.g. {\tt
%     vec4(vp.x, vp.y + 1.0, vp.z, 1.0);}
%   \item {\bf Be Square!} Try to add another triangle to the list of
%     points and make a square shape. You will have to change several
%     variables when setting up the buffer and drawing the shape. Which
%     variables do you need to keep track of for each triangle (hint:
%     not much...).
%   \item Try drawing with {\tt GL\_LINE\_STRIP} or {\tt GL\_LINES} or
%     {\tt GL\_POINTS} instead of triangles. Does it put the lines where you
%     expect? How big are the points by default?
%   \item Try changing the background colour by using {\tt
%     glClearColor()} before the rendering loop. Something grey-ish is
%     usually fairly neutral; {\tt 0.6f, 0.6f, 0.8f, 1.0f}.
%   \item Try creating a second {\tt VAO}, and drawing 2
%     shapes (remember to bind the second VAO before drawing again).
%   \item Try creating a second shader programme, and draw the second shape
%     a different colour (remember to "use" the second shader programme
%     before drawing again).
%   \end{enumerate}
% \end{frame}

\section{Basic maths for 3D Rendering}

\frame{
  \frametitle{Points, Vectors, Transformations, and Basic 3D Geometric Concepts}

  \begin{itemize}
  \item Points and vectors
    \begin{itemize}
    \item[] $(p_x, p_y, p_z)$\\$(v_x, v_y, v_z)$
    \end{itemize}
    \pause
  \item Coordinate system
    \begin{itemize}
    \item Handedness
      \begin{itemize}
      \item[\structure{$\Rightarrow$}] right-handened (RHS) vs. left-handened (LHS)
      \end{itemize}
    \end{itemize}
    \pause
  \item Vector maths
    \begin{itemize}
    \item Dot product
    \item Cross product
    \end{itemize}
    \pause
  \item Homogeneous coordinates
        \begin{itemize}
    \item[] $(p_x, p_y, p_z, 1)$\\$(v_x, v_y, v_z, 0)$
    \end{itemize}
    \pause
  \item Transformation matrices
    \begin{itemize}
    \item Translation
    \item Rotation
    \item Scaling \pause
    \item The {\it Model-View Transform} and the {\it Look-At Matrix}
    \end{itemize}
  \end{itemize}
}

\begin{frame}[fragile]
  \frametitle{Matrix construction and Operators}

  \begin{figure}
    \includegraphics[width=.6\textwidth]{images/matrixXvect}
  \end{figure}

  In C++, with GLM:
  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}
    glm::mat4 myMatrix;
    glm::vec4 myVector;
    // fill myMatrix and myVector somehow

    glm::vec4 transformedVector = myMatrix * myVector; // Order is important!
  \end{minted}

  \quad

  In GLSL :
  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}
    mat4 myMatrix;
    vec4 myVector;
    // fill myMatrix and myVector somehow

    vec4 transformedVector = myMatrix * myVector; // Basically... the same
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Concatenating transformations}

  Let's supposed:
  \begin{enumerate}
  \item Scaling
  \item Rotation
  \item Translation
  \end{enumerate}

  {\tt TransformedVector = TranslationMatrix * RotationMatrix *
    ScaleMatrix * OriginalVector;}\\[0.5cm]

  In C++, with GLM:
  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}
glm::mat4 myModelMatrix = myTranslationMatrix * myRotationMatrix * myScaleMatrix;
glm::vec4 myTransformedVector = myModelMatrix * myOriginalVector;
  \end{minted}

  \quad

  In GLSL :
  \begin{minted}[fontsize=\scriptsize, baselinestretch=1.4]{c++}
mat4 transform = mat2 * mat1;
vec4 out_vec = transform * in_vec;
  \end{minted}
\end{frame}

\frame{
  \frametitle{Coordinate Spaces}

  \vspace*{-0.5cm}
  \begin{table}
    \begin{tabular}{lp{7.5cm}}
      \toprule
      {\bf Coordinate Space} & {\bf What it represents}\\[0.2cm]
      Model Space & Positions relative to a local origin. This is also
                    sometimes known as {\it object space}.\\
      World Space & Positions relative to a global origin (i.e., their
                    location within the world).\\
      View Space & Positions relative to the viewer. This is also
                   sometimes called {\it camera} or {\it eye space}.\\
      Clip Space & Positions of vertices after projection into a
                   non-linear homogeneous coordinate.\\
      NDC~\footnote{NDC: Normalized Device Coordinate} Space
                             & Vertex coordinates are said to be in NDC after
                               their clip-space coordinates have been divided
                               by their own {\tt w} component.\\
      Window Space & Positions of vertices in pixels, relative to the origin of
                     the window.\\
      \bottomrule
    \end{tabular}
    \caption{Common Coordinate Spaces Used in 3D Graphics (table taken from OpenGL
      Superbible)}
  \end{table}
}


\section{Camera model and view frustum}

\frame{
  \frametitle{OpenGL View Frustum}

  \includegraphics[width=0.9\textwidth]{images/culling}
}

\frame{
  \frametitle{OpenGL Camera model}
  \framesubtitle{View frustum and the field of view angle}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/Field_of_view_angle_in_view_frustum}
    \caption{Field of view angle in view frustum \href{http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_05\#mediaviewer/File:Field_of_view_angle_in_view_frustum.png}{\scriptsize (Image by Martin Kraus - CC BY-SA 3.0).}}
  \end{figure}
}

\frame{
  \frametitle{OpenGL Camera model}
  \framesubtitle{Configuring and positioning the view frustum}

  \begin{columns}
    \column{.7\textwidth}
    \includegraphics[width=\textwidth]{images/frustum0}
    \column{.3\textwidth}
    \only<2->{
    \begin{block}{Steps}
      \begin{enumerate}
      \item<2-| alert@2> Position camera
      \item<3-| alert@3> Position model
      \item<4-| alert@4> Select lens
      \end{enumerate}
    \end{block}
    }
  \end{columns}

  \tikz[overlay]{
    \node<2-> at (0.45,2.8) [shape=circle,draw,inner sep=2pt] (char) {1};
    \node<3-> at (5.95,1.85) [shape=circle,draw,inner sep=2pt] (char) {2};
    \node<4-> at (2.8,0.85) [shape=circle,draw,inner sep=2pt] (char) {3};
  }
}

\frame{
  \frametitle{OpenGL Camera model}
  \framesubtitle{Setting the viewing frustum}

  \begin{enumerate}
  \item Move your camera to the location you want to shoot from and point
    the camera the desired direction (\structure{viewing
      transformation}).
    \pause
  \item Move the subject to be photographed into the desired location in the
    scene (\structure{modeling transformation}).
    \pause
  \item Choose a camera lens or adjust the zoom (\structure{projection
      transformation}).
    \pause
  \item Take the picture (apply the transformations).
    \pause
  \item Stretch or shrink the resulting image to the desired picture size
    (\structure{viewport transformation}). For 3D graphics, this also includes
    stretching or shrinking the depth (\structure{depth-range scaling}). This is not to
    be confused with Step 3, which selected how much of the scene to
    capture, not how much to stretch the result.
  \end{enumerate}
}

\frame{
  \frametitle{OpenGL front-end coordinates}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/opengl_coor}
    \caption{Coordinate systems required by OpenGL {\scriptsize (Image from OpenGL Programming Guide 8th ed.)}.}
  \end{figure}
}

\frame{
  \frametitle{OpenGL front-end coordinates}

  \begin{figure}
    \includegraphics[width=0.9\textwidth]{images/opengl_usrcoor}
    \caption{User coordinate systems unseen by OpenGL {\scriptsize (Image from OpenGL Programming Guide 8th ed.)}.}
  \end{figure}
}



\section{Bibliography}

\frame{
  \frametitle{References}

  \beamertemplatebookbibitems
  \nocite{*} %\bibliographystyle{unsrt}
  \printbibliography[keyword=opengl]
}

\frame{
\thispagestyle{empty}

\begin{center}

\ \\% \ \\ \ \\

\includegraphics[width=250px]{images/doom4k2}\\

%\ \\ \ \\ \ \\ \ \\
\textbf{Real-Time Rendering}\\
with OpenGL
\ \\ \ \\ \ \\% \ \\ \ \\

\begin{scriptsize}
\textbf{Emilio Jos\'e Padr\'on Gonz\'alez}\\emilioj@udc.gal\\ \ \\
MUEI, IGM 2018/19
\end{scriptsize}
\end{center}
}

\end{document}
