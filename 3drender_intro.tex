\documentclass[serif, gray, compress]{beamer}

\usepackage{myBeamerStyle}
\setbeamertemplate{itemize subitem}[default]
\setbeamertemplate{itemize subsubitem}[balls]

\usepackage{graphicx}
\usepackage{fancyhdr}

\usepackage[sorting=none, backend=bibtex, defernumbers=true]{biblatex}
\addbibresource{bibliography.bib}

\title{MUEI - IGM 2020/21}
\author{3D Graphics}
\date{}

\begin{document}

\frame{
\thispagestyle{empty}

\begin{center}

\ \\% \ \\ \ \\

\includegraphics[width=0.5\textwidth]{images/utahteapot.png}\\

%\ \\ \ \\ \ \\ \ \\
\textbf{3D Graphics}\\
Rendering realistic$^{\text{*}}$ scenes\\
{\scriptsize $^{\text{*}}$ sometimes non-realistic renders are preferable, though}
\ \\ \ \\ \ \\ \ \\

\begin{scriptsize}
\textbf{Emilio Jos\'e Padr\'on Gonz\'alez}\\emilioj@udc.gal\\ \ \\
MUEI, IGM 2020/21
\end{scriptsize}
\end{center}
}

\AtBeginSection[] {
  \begin{frame}<beamer>
    \frametitle{Outline}
    %\tableofcontents[currentsection, hideothersubsections]
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{frame}<beamer>
  \frametitle{Outline}
  \tableofcontents
\end{frame}


\section{3D Rendering}

\frame{
  \frametitle{Introduction}

  \begin{block}{3D Rendering}
    Process of producing a 2D image from a description of a 3D scene
  \end{block}

  \begin{enumerate}
  \item Mathematically define:
    \begin{itemize}
    \item objects
      \begin{itemize}
      \item 3D geometry \& material
      \item possible animation
      \end{itemize}
    \item materials
      \begin{itemize}
      \item how light interacts
      \end{itemize}
    \item light
    \item camera
    \end{itemize}
  \item Use them to {\bf render} a 2D screen
  \end{enumerate}
}

\frame{
  \frametitle{Types of rendering}

  \begin{itemize}
  \item {\bf 2D} vs. {\bf 3D}
  \item {\bf Photorealistic} vs. {\bf Non-photorealistic rendering
      (NPR)}
  \item \structure<2>{\bf Interactive} vs. \structure<2>{\bf Non-interactive}

    \pause

    \begin{columns}[t]
      \column{.5\textwidth}
      \begin{block}{Real Time Rendering}
        \begin{itemize}
        \item Interactivity
        \item Speed matters
          \begin{itemize}
          \item at least 6 frames per second (FPS)
          \item tipically, a minimum of 18-20 FPS
          \item ideally, between 30-60 FPS
          \end{itemize}
        \item Very optimized software \& hardware
        \end{itemize}
      \end{block}

      \column{.5\textwidth}
      \begin{block}{Offline or Pre-rendering}
        \begin{itemize}
        \item Photorealism
        \item Physically-based rendering
          \begin{itemize}
          \item Complex 3D modeling
          \item Complex interaction of light with matter
          \item Global illumination models
          \end{itemize}
        \end{itemize}
      \end{block}
    \end{columns}
  \end{itemize}
}

\frame{
  \frametitle{Computer Graphics}
  \framesubtitle{Lots of research areas and disciplines}

  \begin{itemize}
  \item {\bf Rendering}
    \begin{itemize}
    \item 2D/3D, Photorealistic/Non-photorealistic, RT/Offline
    \item Volume rendering
    \item Point-based rendering
    \end{itemize}
  \item {\bf Visualization}
    \begin{itemize}
    \item Scientific visualization
    \item Information visualization
    \end{itemize}
  \item {\bf Geometric Processing and Modeling}
    \begin{itemize}
    \item 2D/3D modeling
    \item Multiresolution modeling
    \item Subdivision surfaces
    \end{itemize}
  \item {\bf Computer Vision}
    \begin{itemize}
    \item 2D/3D Reconstruction
    \item Recognition
    \end{itemize}
  \item {\bf Animation}
  \item \ldots
  \end{itemize}
  }

\frame{
  \frametitle{3D Graphics today}

  \begin{columns}
    \column{.37\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=2.5cm]{images/cine}
      \caption{Entertainment industry: movies \& TV}
    \end{figure}

    \column{.37\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=2.5cm]{images/journey}
      \caption{Entertainment industry: videogames}
    \end{figure}

    \column{.37\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=2.5cm]{images/Engine_movingparts}
      \caption{Engineering}
    \end{figure}
  \end{columns}

  \vspace*{-0.25cm}
  \begin{columns}
    \column{.37\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=2.5cm]{images/arquitectura}
      \caption{Architecture and Civil engineering}
    \end{figure}

    \column{.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=2.5cm]{images/medicina} \
      \includegraphics[height=2.5cm]{images/CTSkullImage}
      \caption{Medicine}
    \end{figure}
  \end{columns}
}


\section{Basic Rendering Concepts}

\subsection{Interactivity: frames per second}

\frame{
  \frametitle{Refresh rate \& Frames per second}

  \begin{itemize}
  \item Interactivity: at least 6 \underline{frames per second} (FPS)
  \item Minimum frame rate for videogames: 30 or 60 FPS
    \begin{itemize}
    \item not arbitrary numbers: linked to monitor's refresh rate (usually 60 Hertz)
    \item {\it budget} for getting a new frame: 33.3ms or 16.6ms
    \end{itemize}
  \item Film industry: 24 fps (refresh rates: 48 or 72 Hz)
    \begin{itemize}
    \item motion blur
    \end{itemize}
  \item Graphics programmers have moved towards inverse of FPS:
    \underline{miliseconds per frame}
    \begin{itemize}
    \item exactly time to {\bf update}: {\bf compute} and {\bf display} each new frame
    \item easier to decompose times involved in rendering a frame
    \item better to use maths!
    \end{itemize}
  \end{itemize}
}

\subsection{How an image is generated: the Camera}

\frame{
  \frametitle{Our {\it built-in} visual system}

  \begin{columns}

    \column{.5\textwidth}
    \begin{figure}
      \includegraphics[width=\textwidth]{images/eye}
      \caption{Human eye}
    \end{figure}

    \column{.5\textwidth}
    \begin{exampleblock}{Vision}
      - Eyes detect radiance coming from each direction\\
      - Brain 'creates' image
    \end{exampleblock}

    \pause

    \begin{alertblock}{Our visual system (eyes \& brain) are not:}
    \begin{itemize}
    \item a good light meter
    \item a laser scanner or a sonar
    \end{itemize}
    \end{alertblock}

  \end{columns}

  {\bf Not its purpose:} tries to compensate for shadows and other
      distractions to properly perceive the nature of objects in view.
}

\frame{
  \frametitle{Visual/Optical illusions and perception}
  \framesubtitle{Fool your eyes and trick your mind}

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=0.7\textwidth]{images/ilu1}
      \vspace*{-0.5cm}\caption{Visual Illusion: color}
    \end{figure}

    \vspace*{-0.6cm}\hspace*{-0.5cm}{\footnotesize Cool related video:}\\[-0.1cm]
    \hspace*{-0.5cm}{\tiny \url{https://petapixel.com/2011/08/17/amazing-optical-illusion-shows-that-our-eyes-are-horrible-light-meters}}

    \onslide<2>
    \begin{figure}
      \includegraphics[width=0.45\textwidth]{images/ilu2}
      \caption{Visual Illusion: shape}
    \end{figure}
    \onslide<3>
    \begin{figure}
      \includegraphics[width=0.4\textwidth]{images/ilu3}
      \caption{Visual Illusion: distance}
    \end{figure}
    \onslide<4>
    \begin{itemize}
    \item A source for good optical illusions:

      \begin{center}
        {\bf Best Illusion of the Year Contest}\\
        \url{http://illusionoftheyear.com}
      \end{center}

      \hfil

    \item {\it Magical arts} and {\bf neuroscience} are tightly related
      \begin{itemize}
      \item Neuroscience and magic:\\
        {\footnotesize \url{https://www.magiagalicia.com/blog/neurociencia-y-magia}}

        \quad

      \item {\it ``Los enga\~{n}os de la mente''}, book by Dra Susana Mart\'{i}nez Conde
        (guest in multiple TV's programs, such as REDES: \url{https://www.youtube.com/watch?v=sKnVKR7v1Ho})
      \end{itemize}
    \end{itemize}
  \end{overprint}
}

\frame{
  \frametitle{Visual/Optical illusions and perception}
  \framesubtitle{Photorealism in Computer Graphics}

  \begin{block}{Photorealistic computer graphics}
    In esence... we try to trick our visual system!
  \end{block}

  \begin{itemize}
  \item Some cool realistic renders made with \underline{free software}:\\
    {\scriptsize \url{https://www.blenderguru.com}}

    \quad

  \item Just for curiosity's sake: the other way around, real scenes
    simulating synthetic images:\\ {\scriptsize
      \url{http://skrekkogle.com/projects/still-file}}
  \end{itemize}
}

\begin{frame}{3D Rendering \& Graphics pipeline}

  \begin{block}{3D Rendering}\vspace{0.1cm}
    Process of producing a 2D image from a description of a 3D scene.
  \end{block}

  \begin{itemize}
  \item Specifically, an approach called \emph{rasterization} or
    {\bf scan conversion} is commonly used to transform 3D
    coordinates to 2D pixels.
  \item This process is managed by the graphics pipeline and it
    mostly runs on a GPU.
  \item Graphics pipeline can be divided into two large parts:
    \begin{enumerate}
    \item 3D coordinates $\Rightarrow$ 2D coordinates
    \item 2D coordinates $\Rightarrow$ Colored pixels
    \end{enumerate}
  \end{itemize}

\end{frame}

\begin{frame}{3D Rendering process}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/3Drendering_process}
    \caption{3D Rendering, two main tasks:\\\hspace*{0.15\textwidth} 1. 3D $\rightarrow$ 2D
      transformation (model/view \& projection)\\\hspace*{0.15\textwidth} 2. 2D continuous space
      $\rightarrow$ Pixels (rasterization)}
  \end{figure}

\end{frame}

\frame{
  \frametitle{Graphics Pipeline}

  \begin{itemize}
  \item The {\bf data-flow} in a typical rendering pipeline can be
    basically depicted as:
    \begin{center}
\hspace*{-1.5cm} {\small {\bf Application} -> {\bf Geometry Transformations} ->
        {\bf Rasterization}} -> \framebox[1.1\width]{Screen}
    \end{center}

    \pause

    \begin{enumerate}
    \item Application sends 3D primitives to GPU, tipically triangles
    \item These triangles are modified by camera's view of the world
      along with whatever modelling transform is applied
      \begin{itemize}
      \item[->] The location of each triangle on the screen is computed
      \item[->] View frustum culling is applied\\
        + totally inside the frustum -> rasterization\\
        + partially inside the frustum -> clipped and rasterization
      \end{itemize}
    \item Identify all the pixels whose centers are inside a triangle -> it fills in the triangle
    \end{enumerate}

  \end{itemize}
}

\frame{
  \frametitle{View frustum: the viewing volume}

  \begin{figure}
    \includegraphics[width=0.7\textwidth]{images/viewfrustum}
  \end{figure}

  \begin{itemize}
  \item Idea of {\bf view frustum}: volume of virtual world captured by camera
    \begin{itemize}
    \item Exact shape of this volume depends on what kind of camera lens is
      being simulated
    \item {\bf View frustum culling}: process of removing objects that lie
      completely outside view frustum from rendering process
    \item Visible objects are projected on the near plane
    \end{itemize}
  \end{itemize}
}


\frame{
  \frametitle{View frustum culling}

  \includegraphics[width=0.9\textwidth]{images/culling}
}

\frame{
  \frametitle{How a (virtual) camera works}
  \framesubtitle{Basic starting point: Pinhole camera}

  % Pinhole camera = Cámara estenopeica

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics{images/pinhole_camera}
      \caption{Basic idea: \emph{pinhole camera}~\footnote{Pinhole
          camera = Cámara estenopeica}}
    \end{figure}
    \onslide<2>
    \begin{figure}
      \includegraphics[width=0.5\textwidth]{images/pinhole_camera}
    \end{figure}
    \begin{figure}
      \includegraphics[width=0.7\textwidth]{images/simulated_pinhole_camera}
      \caption{Simulated pinhole camera: film plane in front of the
        pinhole, same distance}
    \end{figure}
    \begin{tikzpicture}[remember picture,overlay]%
      \draw[->, red, dashed, very thick] (3.65,7.15) -- (5,4.6);
    \end{tikzpicture}
    \onslide<3>
    \begin{figure}
      \includegraphics[width=0.5\textwidth]{images/pinhole_camera}
    \end{figure}
    \begin{figure}
      \includegraphics[width=0.7\textwidth]{images/viewfrustum}
      \caption{Simulated pinhole camera: film plane in front of the
        pinhole, same distance}
    \end{figure}
    \begin{tikzpicture}[remember picture,overlay]%
      \draw[->, red, dashed, very thick] (3.65,7.15) -- (5,4.6);
    \end{tikzpicture}
  \end{overprint}
}

\frame{
  \frametitle{How a (virtual) camera works}
  \framesubtitle{View frustum: the viewing volume}

  \begin{figure}
    \includegraphics[width=0.7\textwidth]{images/viewfrustum}
  \end{figure}

  \begin{itemize}
  \item Idea of {\bf view frustum}: volume of virtual world captured by camera
    \begin{itemize}
    \item Exact shape of this volume depends on what kind of camera lens is
      being simulated
    \item {\bf View frustum culling}: process of removing objects that lie
      completely outside view frustum from rendering process
    \end{itemize}

    \pause

  \item What color value is displayed at each point in the image?
    \begin{itemize}
    \item Amount of light traveling from the image point (film) to the eye
    \end{itemize}
  \end{itemize}
}

\subsection{A 3D Scene}

\frame{
  \frametitle{Scene and objects}
  \framesubtitle{Defining a virtual world}

  {\bf Objects} in the virtual scene
  \begin{itemize}
  \item described by some {\bf 3D geometry}
  \item have some {\bf material} description that says how {\bf light} interacts
  \item possibly animated over {\bf time}
  \end{itemize}

  \pause

  \begin{block}{3D Scene}
    \begin{itemize}
    \item Objects: 3D Geometry and material
    \item Lights
    \item Camera
    \end{itemize}
  \end{block}
}

\subsection{The Light}

\frame{
  \frametitle{Light}
  \framesubtitle{Physical \& virtual}

  \begin{itemize}
  \item {\bf How light travels in the real world}: really complex modeling
    \begin{itemize}
    \item Lots of photons per second interacting (being reflected,
      absorbed\ldots) with materials and reaching the camera
    \item Simulating light behavior involves a lot of physics
      \begin{itemize}
      \item[] \ldots but it is \underline{key} for realism
      \end{itemize}
    \item Different simplifications and assumptions are common in C\&G
    \end{itemize}
  \end{itemize}

  \begin{overprint}

    \onslide<1>
    \begin{figure}
      \hspace*{-0.8cm}\includegraphics[width=1.15\linewidth]{images/298px-Render_Types-0-0}
      \caption{Example of different illumination models applied to the same 3D scene
        {\scriptsize ~~(By Maximilian Sch\"{o}nherr - Own work, CC BY-SA 3.0,
          \url{https://commons.wikimedia.org/w/index.php?curid=15622493})}}
    \end{figure}

    \onslide<2>
    \begin{block}{}
    \begin{itemize}
    \item \structure{Photorealistic rendering} tries to model
      physically-based light-matter interaction
      \begin{itemize}
      \item Offline rendering $\Rightarrow$ not real time
      \end{itemize}

    \item \structure{Real time rendering} uses simpler shading/lighting models
      \begin{itemize}
      \item Interactivity needs high frame rate
      \end{itemize}
    \end{itemize}
    \end{block}
  \end{overprint}
}

\frame{
  \frametitle{Realism}
  \framesubtitle{Physically-based light modelling: key for getting realistic renders}

  \vspace*{-0.2cm}
  \begin{figure}
    \includegraphics[width=0.8\linewidth]{images/640px-Glasses_800_edit}
    \vspace*{-0.2cm}
    \only<1>{\caption{An image created by using POV-Ray 3.6
        {\scriptsize (By Gilles Tran - \url{http://www.oyonale.com/modeles.php?lang=en\&page=40},
          Public Domain, \url{https://commons.wikimedia.org/w/index.php?curid=745313})}}
    }
    \only<2>{\\[0.2cm]\structure{$\Rightarrow$} Selection of POV-Ray renders:\\
    {\footnotesize \url{http://hof.povray.org}}}
  \end{figure}
}

   % \item {\bf How  we simplify this in our virtual world?}\\

   %  In computer graphics we make a number of simplyfing assumptions.

   %  Multiple possibilities:
   %  \begin{itemize}
   %  \item One way: Reversing the process ({\bf ray tracing})
   %    \begin{itemize}
   %    \item start from the camera
   %    \item sum up direct lighting
   %    \item no shadows (simplest assumption) or hard shadows
   %    \end{itemize}
   %  \item Even simplier: No lights, using solid colors

   %    \pause

   %  \item Or using {\bf scan-line rendering} instead of ray tracing
   %    \begin{itemize}
   %    \item a.k.a. \emph{rasterization}
   %    \end{itemize}
   %  \end{itemize}

\subsection{3D Rendering Techniques}

\frame{
  \frametitle{\insertsubsection}
  \framesubtitle{Render a 3D scene onto a 2D surface}

  \begin{figure}
    \includegraphics[width=0.7\linewidth]{images/pixels_projection}
  \end{figure}

  \begin{itemize}
  \item We have {\bf 3D objects}, we want a {\bf 2D image} (pixels)
    from a specific point of view
  \item Two main techniques:
    \begin{itemize}
    \item[] \structure{Rasterization} vs. \structure{Ray Tracing}
    \end{itemize}
  \item Different approaches to solve the {\bf visibility} or
    \emph{hidden surface problem} for rendering the image
    \begin{itemize}
    \item both algorithms apply geometry techniques to solve it
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Rasterization vs. Ray Tracing}
  \setbeamertemplate{itemize item}[triangle]%

  \begin{columns}
    \column{.5\textwidth}
    % \begin{figure}
    %   \centering
    %   \includegraphics[width=0.7\linewidth]{images/raster}
    %   %\caption{Rasterization}
    % \end{figure}
    \begin{block}{Rasterization\\ (aka Scan-line Rendering)}
      \underline{\it Object} centric
      \begin{itemize}
      \item Projection geometry forward
      \end{itemize}
    \end{block}

    \column{.5\textwidth}
    % \begin{figure}
    %   \centering
    %   \includegraphics[width=0.7\linewidth]{images/rayt}
    %   %\caption{Ray Tracing}
    % \end{figure}
    \begin{block}{Ray Tracing}
      \underline{\it Image} centric
      \begin{itemize}
        \item shoot rays from the camera into the scene (we start from the image)
        \item project image samples backwards
        \end{itemize}
    \end{block}
  \end{columns}

  \vspace*{0.25cm}

  \begin{itemize}
  \item Lots of differences and pros/cons. One important: dealing with
    {\bf visibility} and {\bf depth}.
  \item GPU's architecture follows a scan-line approach
  \item Ray Tracing is more suitable for global illumination models
  \end{itemize}
}

\frame{
  \frametitle{Ray Tracing}
  \framesubtitle{as a natural method for computing illumination in render}

  \begin{figure}
    \includegraphics[width=0.9\textwidth]{images/ray_trace_diagram.pdf}
    \caption{This diagram illustrates the simple ray tracing algorithm for
      rendering an image \href{http://commons.wikimedia.org/wiki/File:Ray_trace_diagram.svg}{\scriptsize (Image by Henrik - GFDL 1.2)}.}
  \end{figure}
}

\subsection{The Render Pipeline}

\frame{
  \frametitle{Pipelining}

  \begin{figure}
    \includegraphics[width=\textwidth]{images/pipeline_no}
    \caption{Monolithic Task}
  \end{figure}

  \pause

  \begin{figure}
    \includegraphics[width=\textwidth]{images/pipeline_si}
    \caption{3-Stage Pipeline}
  \end{figure}

  \pause

  \begin{itemize}
  \item Advantage of a {\bf pipeline}: each part of the pipeline can work on
    as a separate task at the same time
    \begin{itemize}
    \item A new item is ready (come out of the pipeline) each pipeline cycle
    \item The lowest stage determines how fast anything is going to
      come out of the pipeline: \underline{bottleneck}
      \begin{itemize}
      \item {\bf Stalling}: one stage is stopping the pipeline
      \item {\bf Starving}: stages after the bottleneck are idle
      \end{itemize}
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{The Graphics Pipeline}

  \begin{itemize}
  \item A GPU uses a rendering process called \emph{rasterization} or {\bf scan
    conversion}
    \begin{center}
      {\small {\bf Application} -> {\bf Camera \& Model Transform} -> {\bf Rasterization}}
    \end{center}
    \begin{enumerate}
    \item Application sends objects to the GPU, tipically 3D
      triangles
    \item These triangles are modified by camera's view of the world
      along with whatever modelling transform is applied\\
      -> The location of each triangle on the screen is computed
      \begin{itemize}
      \item totally inside the frustum -> rasterization
      \item partially inside the frustum -> clipped and rasterization
      \end{itemize}
    \item Identify all the pixels whose centers are inside a triangle -> it fills in the triangle
    \end{enumerate}
  \item In a GPU pipeline is combined with parallelism to get extremely fast rendering speeds
  \end{itemize}
}

\frame{
  \frametitle{The Graphics Pipeline}

  \begin{overprint}
    \onslide<1>
    \begin{figure}
      \includegraphics[width=\linewidth]{images/pipeline_coarse}
      \caption{Basic 3D Rendering Pipeline}
    \end{figure}

    \onslide<2>
    \begin{figure}
      \includegraphics[width=0.5\linewidth]{images/pipeline_coarse}
    \end{figure}

    \begin{figure}
      \hspace*{-0.5cm}\includegraphics[width=1.1\linewidth]{images/pipeline_geometry}
      \caption{Tasks in the Geometry Stage}
    \end{figure}
  \end{overprint}
}

\frame{
  \frametitle{The Graphics Pipeline}
  \framesubtitle{Classic rasterizing problem: Depth and visibility}

  \begin{itemize}
  \item Two classic approaches to deal with {\bf depth} and {\bf visibility}
    during rasterization:
    \begin{itemize}
    \item Painter's algorithm
    \item Z-Buffer: Usual approach nowadays
    \end{itemize}
  \end{itemize}

  \pause

  \begin{alertblock}{How to deal with this in {\bf ray tracing}}
    No problem: just need to find the nearest object intersecting each ray
    \begin{itemize}
    \item[\checkmark] the ray casting process naturally solves any visibility problem
    \end{itemize}
  \end{alertblock}
}

\subsection{3D Render APIs}

\frame{
  \frametitle{3D Graphics API}
  \framesubtitle{Classical scenario: OpenGL vs. DirectX/Direct3D}

  \begin{itemize}
  \item OpenGL (created in 1992) $\rightarrow$ $\cdots$  $\rightarrow$
    2.1  $\rightarrow$ {\bf 3.0}  $\rightarrow$ $\cdots$
    $\rightarrow$ 4.6
    \begin{itemize}
    \item The Khronos Group: {\scriptsize \url{https://www.khronos.org}}
    \end{itemize}
  \item OpenGL  $\Rightarrow$ OpenGL ES  $\Rightarrow$ WebGL
    \begin{itemize}
    \item Cross-language, multi-platform API
    \item State-based API
      \begin{enumerate}
      \item Set up exactly how you want the GPU to do something
      \item Send a geometry to render under these conditions
      \end{enumerate}
    \item Advantage: pretty fine grained control over the GPU
    \item Disadvantage: lots of lines of code, even for the simplest
      scenes
      \begin{itemize}
      \item Solution in WebGL: \hyperlink{3js}{\textit{Three.JS library}}
      \item Check your browser's WebGL support: {\scriptsize \url{http://get.webgl.org}}
      \end{itemize}
    \end{itemize}
  \item DirectX/Direct3D
    \begin{itemize}
    \item Microsoft's alternative (only MS' platforms)
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{3D Graphics API}
  \framesubtitle{Cutting-edge technologies}

  \begin{block}{}\centering
    Low-overhead and low CPU Usage APIs
  \end{block}

  \begin{itemize}
  \item AMD's Mantle (2013) {\footnotesize {\bf -- deprecated}, work contributed to Vulkan}
  \item MS's Direct3D 12
    \begin{itemize}
    \item Only for Microsoft OSs, as usual
    \end{itemize}
  \item Apple's Metal
    \begin{itemize}
    \item iOS and OSX
    \end{itemize}
  \item {\it The next generation OpenGL initiative} $\Rightarrow$ {\tt
      glNext} $\Rightarrow$ {\bf Vulkan}
    \begin{itemize}
    \item Cross-platform
    \item Khronos Group, developed from AMD's Mantle
    \item Released in February 2016
    \item Low level alternative to OpenGL (not a replacement, at least for now)
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Modern OpenGL Pipeline}

  \begin{figure}
    \centering
     \includegraphics[height=0.9\textheight]{images/RenderingPipeline}
    \caption{OpenGL Rendering Pipeline}
  \end{figure}
}


\section{Useful Resources}

\subsection{Smart Libraries for Computer Graphics}

\frame{
  \frametitle{Open Source CG Libs}
  \framesubtitle{3D Geometry \& Maths}

  \begin{overprint}
    \onslide<1>
    \begin{description}
    \item[CGAL] Provides an easy access to efficient and reliable
      geometric algorithms in the form of a C++ library.
      \url{www.cgal.org}
    \item[VCGLib] The Visualization and Computer Graphics Library is an
      open source portable C++ templated library for manipulation,
      processing and displaying with OpenGL of polygonal meshes.
      \url{http://vcg.sf.net}
    \item[PCL]  The Point Cloud Library is a large scale, open project
      for 2D/3D image and point cloud processing.\\
      \url{http://pointclouds.org}
    \end{description}

    \onslide<2>
    \begin{description}
    \item[Eigen] C++ template library for linear algebra: matrices,
      vectors, numerical solvers, and related algorithms.
      \url{http://eigen.tuxfamily.org}
    \item[GLM] OpenGL Mathematics. C++ mathematics library for 3D software based
      on the OpenGL Shading Language (GLSL) specification.
      \url{http://glm.g-truc.net}
    \item[g2o] C++ framework for optimizing graph-based nonlinear error
      functions.
      \url{http://openslam.org/g2o.html}
    \end{description}
  \end{overprint}
}

\frame{
  \frametitle{Open Source CG Libs}
  \framesubtitle{Image Libraries}

  \begin{description}
  \item[GLI] OpenGL Image. Small cross-platform C++ image library for graphics
    software based on the OpenGL Shading Language (GLSL) specification and
    released under the MIT license.
    \url{http://gli.g-truc.net}
  \item[DevIL] A full featured cross-platform image library.
    \url{http://openil.sourceforge.net}
  \item[ResIL] Resilient Image Library - successor to DevIL
    \url{http://resil.sourceforge.net}
  \item[FreeImage] Cross-platform library project for developers who would like
    to support popular graphics image formats like PNG, BMP, JPEG, TIFF and
    others as needed by today's multimedia applications.
    \url{http://freeimage.sourceforge.net}
  \end{description}
}

\frame{
  \frametitle{Open Source CG Libs}
  \framesubtitle{Scientific Visualization}

  \begin{description}
  \item[VTK] The Visualization Toolkit is an open-source, freely
    available software system for 3D computer graphics, image
    processing and visualization.
    \url{http://www.vtk.org}
  \item[VisTrails] Scientific workflow and provenance
    management system that supports data exploration and visualization.\\
    \url{http://www.vistrails.org}
  \item[OpenCV] Cross-platform library of programming functions mainly
    aimed at real-time computer vision. It focuses mainly on real-time
    image processing.
    \url{http://opencv.org}
  \end{description}
}

\frame{
  \frametitle{Open Source CG Libs}
  \framesubtitle{Graphics Toolkits \& Multimedia Engines}

  \vspace*{-0.3cm}
  \begin{description}
  \item[SDL] Simple DirectMedia Layer is a cross-platform development
    library designed to provide low level access to audio, keyboard,
    mouse, joystick, and graphics hardware via OpenGL and Direct3D.\\[0.1cm]
    SDL is written in C, works natively with C++, and there are
    bindings available for several other languages, including C\# and
    Python. SDL 2.0 supports Windows, Mac OS X, Linux, iOS, and Android.
    \url{http://www.libsdl.org}
  \item[SFML] Simple and Fast Multimedia Library provides a simple
    interface to the various components of a computer to ease the
    development of games and multimedia applications. It is written in
    C++ and cross-platform.
    \url{http://www.sfml-dev.org}
  \item[OpenSceneGraph] Cross-platform High performance 3D graphics
    toolkit written in C++ and OpenGL.
    \url{http://www.openscenegraph.org}
  \end{description}
}

\frame{
  \frametitle{Open Source CG Libs}
  \framesubtitle{Game Engines}

  \begin{description}
  \item[Godot] Following the trail of UE
    \url{http://godotengine.org}
  \item[Ogre3D] Big 3D engine (Modern C++: STL, RTTI\ldots)
    \url{http://www.ogre3d.org}
  \item[Armory3D] Promising. The team behind ArmorPaint
    \url{https://armory3d.org}
  \item[Irrlicht] Light 3D engine (Clean C++, no STL)
    \url{http://irrlicht.sourceforge.net} \url{http://www.irrlicht3d.org}
  \item[Horde3D] Small engine with modern design
    \url{http://www.horde3d.org}
  % \item[BlendELF] Modern design \& modern effects: DOF, HDR\ldots\\
  %   \url{http://github.com/jesterKing/BlendELF}
  \item[Panda3D] Mature game engine for Python and C++
    \url{http://www.panda3d.org}
  % \item[Crystal Space] Old design and a bit out-of-date
  %   \url{http://www.crystalspace3d.org}
  \item[Torque3D] Comercial engine now released as Open Source
    \url{http://torque3d.org}\\
    \begingroup
    \leftskip24em
    \rightskip\leftskip
    \makebox[0pt][r]{\url{http://www.garagegames.com/products/torque-3d}}
    \par
    \endgroup

  \end{description}
}

\frame{
  \frametitle{CG Libs}
  \framesubtitle{Game Engines -- out of free software world}

  \begin{description}
  \item[Unity] Versatility
    \url{https://unity3d.com}

    \quad

  \item[Unreal Engine] Cutting-edged 3D development
    \url{https://www.unrealengine.com}
  \end{description}
}

\frame{
  \frametitle{Open Source Modeling Software}

  \begin{description}
  \item[Blender] Free software beast!
    \url{http://www.blender.org}

  \item[ArmorPaint] Physically-based texture painting
    \url{https://armorpaint.org}

  \end{description}
}

\frame{
  \frametitle{Open Source Physically-based Renderers}

  \begin{description}
  \item[PBRT] Engine and book all together!
    \url{https://pbrt.org} \url{https://github.com/mmp/pbrt-v4}

  \item[Flux] Like PBRT, but no educational approach
    \url{https://github.com/JulianThijssen/Flux}

  \item[Mitsuba2] Research oriented
    \url{https://www.mitsuba-renderer.org}
  \end{description}
}

\frame{
  \frametitle{Open Source CG Libs}
  \framesubtitle{3D Rendering on the web (WebGL frameworks)}

  \begin{description}
  \item[X3D] Royalty-free open standards file format and run-time
    architecture to represent and communicate 3D scenes and objects
    using XML. (VRML successor)
    {\footnotesize \url{http://www.web3d.org}}
  \item[X3DOM] Experimental open-source framework and runtime to
    fulfill the current HTML5 specification for declarative 3D
    content, allowing the inclusion of X3D elements as part of any HTML5 DOM tree.
    {\footnotesize \url{http://www.x3dom.org}}
  \item[Three.js] The most mature option. Modular rendering interface
    allowing it to be used with SVG and HTML5's canvas element in
    addition to WebGL.\hypertarget{3js}{}
    {\footnotesize \url{http://github.com/mrdoob/three.js}}\\
    $\Rightarrow$ Lots of examples in {\footnotesize \url{http://threejs.org}}
  \item[SceneJS] More CAD oriented.
    {\footnotesize \url{http://scenejs.org}}
  \item[SpiderGL] Promising...
    {\footnotesize \url{http://www.spidergl.org}}
  \end{description}
}

\subsection{Bibliography}

\frame{
  \frametitle{References}
  \framesubtitle{Real Time Rendering}

  \beamertemplatebookbibitems
  \nocite{*} %\bibliographystyle{unsrt}
  \printbibliography[keyword=RT]
}

\frame{
  \frametitle{References}
  \framesubtitle{Physically-based Rendering}

  \beamertemplatebookbibitems
  \printbibliography[keyword=PBR]
}

\frame{
  \frametitle{References}
  \framesubtitle{Lots of online docs and resources}

  \setbeamertemplate{bibliography item}[online]
  \printbibliography[keyword=online]
}

\frame{
\thispagestyle{empty}

\begin{center}

  Thank you very much for your attention!\\[0.5cm]
  All questions and comments are welcome :-)
\ \\ \ \\% \ \\

\includegraphics[width=0.5\textwidth]{images/utahteapot.png}\\

%\ \\ \ \\ \ \\ \ \\
\textbf{3D Graphics}\\
Rendering realistic$^{\text{*}}$ scenes\\
{\scriptsize $^{\text{*}}$ sometimes non-realistic renders are preferable, though}
\ \\ \ \\% \ \\ \ \\

\begin{scriptsize}
\textbf{Emilio Jos\'e Padr\'on Gonz\'alez}\\emilioj@udc.gal\\
MUEI, IGM 2020/21
\end{scriptsize}
\end{center}
}

\end{document}
